import { ChangeEventHandler} from "react";
import './search-box.styles.css';

type SearchBoxProps = {
  placeholder: string;
  handleChange: ChangeEventHandler<HTMLInputElement>;
}

export const SearchBox = ({ placeholder, handleChange }: SearchBoxProps) => (
  <input type="search" className="search"
    placeholder={placeholder}
    onChange={handleChange} />
);